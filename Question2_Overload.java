package week2.assignment;

public class Question2_Overload
{

    public static void display(String a)
    {
        System.out.println("Static method called."+a);
    }

    public static void display(int x)
    {
        System.out.println("An overloaded static method called."+x);
    }

    public static void main(String args[])
    {

        Question2_Overload.display("Praveer");
        Question2_Overload.display(160);
    }
}
