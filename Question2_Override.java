
package week2.assignment;


public class Question2_Override
{
    public static void main(String args[])
    {
        ParentClass pc = new ChildClass();

        pc.display(5);
    }
}
//parent class
class ParentClass
{

    public static void display(int b)
    {
        System.out.printf("display() method of the parent class."+b);
    }
}
//child class
class ChildClass extends ParentClass
{

    public static void display(int a)
    {
        System.out.println("Overridden static method in Child Class in Java"+a);
    }
}